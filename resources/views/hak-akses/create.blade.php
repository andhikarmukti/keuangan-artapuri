@extends('layouts.main')

@section('container')
<a href="/">kembali</a>
<form action="/hak-akses" method="get">
    <div class="col-3 mb-1">

        @if (session()->has('berhasil'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('berhasil') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif

        <select class="form-select mt-5" name="user_id">
            <option selected disabled>Pilih Nama</option>
            @foreach ($user as $u)
            <option value="{{ $u->id }}" @if($u->id == request('user_id')) selected @endif>{{ $u->name }}</option>
            @endforeach
          </select>
    </div>
    <button class="btn btn-primary mb-4" type="submit">Search</button>
</form>

@if(request('user_id'))
<form action="/hak-akses" method="post">
    @csrf
    <table class="table table-hover">
        <thead>
          <tr>
            <th scope="col">Nama</th>
            <th scope="col">Hak Akses</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($entitas as $e)
            <tr>
              <th scope="row">{{ $e->entitas }}</th>
              <th><input type="checkbox" value="{{ $e->entitas }}" name="{{ $e->entitas }}" @foreach ($hak_akses as $h_a)
                  @if($h_a->jenis_akses == $e->entitas)
                      checked
                  @endif
              @endforeach></th>
              <input type="hidden" value="{{ request('user_id') }}" name="user_id">
            </tr>
            @endforeach
        </tbody>
      </table>
      <button class="btn btn-warning" type="submit">Submit</button>
</form>
@endif

@endsection