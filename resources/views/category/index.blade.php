@extends('layouts.main')

@section('container')

    <h1>Tambah Kategori</h1>

    <div class="col-6 mb-4">
        <form action="/tambah-kategori" method="post">
            @csrf
            <div class="mb-3">
                <label for="divisi" class="form-label">Entitas</label>
                <select class="form-select" name="entitas" id="entitas">
                    <option value="" selected disabled>Pilih Kategori..</option>
                    @foreach ($data as $d)
                        <option value="{{ $d->entitas }}">{{ $d->entitas }}</option>
                    @endforeach
                </select>
            </div>
            <div class=" mb-3">
                <label for="kategori" class="form-label">Kategori</label>
                <input type="text" class="form-control" id="kategori" name="kategori">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    <a href="/home">kembali</a>

@endsection
