@extends('layouts.main')

@section('container')

    <h1>Tambah Divisi</h1>

    <div class="col-6 mb-4">
        <form action="/tambah-entitas" method="post">
            @csrf
            <div class="mb-3">
                <label for="entitas" class="form-label">Entitas</label>
                <input type="text" class="form-control" id="entitas" name="entitas">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

    <a href="/">kembali</a>

@endsection
