@extends('layouts.main')

@section('container')

<marquee scrollamount="20" class="mt-3"><h1>{{ $greetings }}, {{ auth()->user()->name }}!</h1></marquee>

<h3 class="mt-5 mb-3">Data Keuangan Artapuri</h3>
{{-- <a href="/cashflow"><button class="btn btn-primary mb-4">Tambah Data</button></a> --}}
<a href="/analytic"><button class="btn btn-primary mb-4">Analytic</button></a>

{{-- <form action="/" method="GET" class="col-4">
    <div class="input-group mb-3">
        <input type="text" class="form-control" id="search" name="search" placeholder="Search.." value="{{ request('search') }}">
        <button class="btn btn-primary" type="submit">Search</button>
        <a href="/"><button class="btn btn-danger" type="button">Clear</button></a>
    </div>
</form> --}}

@if (session()->has('suksesKategori'))
  <div class="alert alert-success alert-dismissible fade show" role="alert">
    {{ session('suksesKategori') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif

@if (session()->has('suksesEntitas'))
  <div class="alert alert-success alert-dismissible fade show" role="alert">
    {{ session('suksesEntitas') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif

@if (session()->has('suksesTambahData'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
  {{ session('suksesTambahData') }}
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

@if (session()->has('suksesUpdateData'))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
  {{ session('suksesUpdateData') }}
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

@if (session()->has('suksesDelete'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
  {{ session('suksesDelete') }}
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

@if (session()->has('suksesPayment'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
  {{ session('suksesPayment') }}
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

@if (session()->has('tolakAkses'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{ session('tolakAkses') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

<div class="col-6 mt-5 mb-4">
    <h3>Input Pengeluaran</h3>
  <form action="/cashflow" method="post">
      @csrf
      <div class="mb-3">
          <label for="tanggal" class="form-label">Tanggal</label>
          <input type="date" class="form-control @error('tanggal') is-invalid @enderror" id="tanggal" name="tanggal" value="{{ old('tanggal', $now) }}">
          @error('tanggal')
              <div class="invalid-feedback">{{ $message }}</div>
          @enderror
      </div>
      <div class="mb-3">
          <label for="jenis" class="form-label">Jenis</label>
          <select class="form-select @error('jenis') is-invalid @enderror" name="jenis" id="jenis">
              <option selected value="{{ old('jenis') }}">{{ old('jenis') }}</option>
              <option value="Pemasukan">Pemasukan</option>
              <option value="Pengeluaran">Pengeluaran</option>
          </select>
          @error('jenis')
              <div class="invalid-feedback">{{ $message }}</div>
          @enderror
      </div>
      <div class="mb-3">
          <label for="entitas" class="form-label">entitas</label>
          <select class="form-select @error('entitas') is-invalid @enderror" name="entitas" id="entitas">
              <option selected value="" disabled>--</option>
              @if(auth()->user()->role = 'admin')
              @foreach ($enttitas_admin as $e_a)
              <option value="{{ $e_a->entitas }}" {{ old('entitas') == $e_a->entitas ? 'selected' : "" }}>{{ $e_a->entitas }}</option>
              @endforeach
              @endif
              @foreach($entitas as $e)
              <option value="{{ $e->jenis_akses }}" {{ old('entitas') == $e->jenis_akses ? 'selected' : "" }}>{{ $e->jenis_akses }}</option>
              @endforeach
            </select>
            @error('entitas')
              <div class="invalid-feedback">{{ $message }}</div>
          @enderror
      </div>
      <div class="mb-3">
          <label for="kategori" class="form-label">Kategori</label>
          <select class="form-select @error('kategori') is-invalid @enderror" name="kategori" id="kategori">
              <option selected value="{{ old('kategori') }}">{{ old('kategori') }}</option>
            </select>
            @error('kategori')
              <div class="invalid-feedback">{{ $message }}</div>
          @enderror
      </div>
      <div class="mb-3">
          <label for="currency" class="form-label">Currency</label>
          <select class="form-select @error('currency') is-invalid @enderror" name="currency" id="currency">
              <option selected disabled value="{{ old('currency') }}">{{ old('currency') }}</option>
              <option @if(old('currency') == 'Rupiah') selected @elseif(!old('currency')) selected @endif value="Rupiah">Rupiah</option>
              <option {{ old('currency') == 'Dollar' ? 'selected' : "" }} value="Dollar">Dollar</option>
          </select>
          @error('currency')
              <div class="invalid-feedback">{{ $message }}</div>
          @enderror
      </div>
      <div class="mb-3">
          <label for="nominal" class="form-label">Nominal</label>
          <div class="input-group mb-3">
              <span class="input-group-text" id="satuanNominal">Rp</span>
              <input type="int" class="form-control @error('nominal') is-invalid @enderror" placeholder="Nominal" name="nominal" id="nominal" value="{{ old('nominal') }}">
              @error('nominal')
              <div class="invalid-feedback">{{ $message }}</div>
          @enderror
          </div>
      </div>
      <div class="mb-3">
          <label for="payment" class="form-label">Payment</label>
          <select class="form-select @error('payment') is-invalid @enderror" name="payment" id="payment">
              <option selected value="{{ old('payment') }}">{{ old('payment') }}</option>
              @foreach ($payment as $p)
              <option value="{{ $p->payment }}">{{ $p->payment }}</option>
              @endforeach
          </select>
          @error('payment')
              <div class="invalid-feedback">{{ $message }}</div>
          @enderror
      </div>
      <div class="mb-3">
          <label for="keterangan" class="form-label">Keterangan</label>
          <textarea class="form-control @error('keterangan') is-invalid @enderror" id="keterangan" name="keterangan">{{ old('keterangan') }}</textarea>
          @error('keterangan')
              <div class="invalid-feedback">{{ $message }}</div>
          @enderror
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>

<script>
    $(document).ready(function(){
        $('#search').keyup(function(){
            var nilai = $(this).val();
            console.log(nilai)
            $.ajax({
                type : 'GET',
                url : '/home?nilai='+nilai,
                dataType : 'JSON',
                success:function(result){
                    console.log('ok')
                }
            });
        });
    });

    $('.delete').click(function(e){

    e.preventDefault();
    var href = $(this).attr('href');

    Swal.fire({
    title: 'Yakin mau dihapus?',
    text: "Dia akan pergi untuk selamanya jika kamu hapus :(",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Pokoknya hapus!',
    cancelButtonText: 'Tidaaaak!'
    }).then((result) => {
    if (result.isConfirmed) {
      Swal.fire(
        'Deleted!',
        'Your file has been deleted.',
        'success'
        )
        document.location.href = href;
    }
    })
    })

    $('#entitas').change(function(){
            var nilai = $(this).val();
            $.ajax({
                type : 'GET',
                url : '/cashflow?nilai='+nilai,
                dataType : 'JSON',
                success : function(result){
                    console.log(result)
                    if(result.length > 0){
                        $('#kategori').empty();
                        $('#kategori').html(`
                            <option selected disabled >Pilih Kategori</option>
                        `);
                    $.each(result, function(index, element){
                            $('#kategori').append(`
                                <option value = '`+ element +`'>`+ element +`</option>
                            `)
                        })
                    }else{
                        $('#kategori').html(`
                            <option selected disabled>Tidak Ada Kategori</option>
                        `)
                    }
                }
            })
       });

        // Format mata uang.
        // $( '#nominal' ).mask('000.000.000', {reverse: true});

       $('#currency').change(function(){
            var currency = $(this).val();
            console.log(currency);

           if(currency == 'Rupiah'){
               $('#satuanNominal').html(`
                   <span id="satuanNominal">`+ 'Rp' +`</span>
               `)
           }else {
            $('#satuanNominal').html(`
                   <span id="satuanNominal">$</span>
               `)
           }
       });
</script>

<script type="text/javascript">
		
    var nominal = document.getElementById('nominal');
    nominal.addEventListener('keyup', function(e){
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        nominal.value = formatRupiah(this.value);
    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        nominal     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            nominal += separator + ribuan.join('.');
        }

        nominal = split[1] != undefined ? nominal + ',' + split[1] : nominal;
        return prefix == undefined ? nominal : (nominal ? '' + nominal : '');
    }
</script>

@endsection