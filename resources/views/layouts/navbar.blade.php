<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="/">Home</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Opsi
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="dropdown-item" href="/tambah-kategori">Tambah Kategori</a></li>
              <li><a class="dropdown-item" href="/tambah-payment">Tambah Payment</a></li>
              @if(auth()->user()->role == 'admin')
              <li><a class="dropdown-item" href="/tambah-entitas">Tambah Entitas</a></li>
              <li><a class="dropdown-item" href="/hak-akses">Pengaturan Hak Akses</a></li>
              <li><a class="dropdown-item" href="/activate-account">Pengaturan Akun User</a></li>
              @endif
            </ul>
            </li>
          <li class="nav-item">
              <form action="{{ route('logout') }}" method="post">
                @csrf
                <button class="nav-item" href="#" type="submit">Logout</button>
            </form>
          </li>
        </ul>
      </div>
    </div>
  </nav>