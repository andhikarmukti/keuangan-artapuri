@extends('layouts.main')

@section('container')

    <h1>Tambah Payment</h1>

    <div class="col-6 mb-4">
        <form action="/tambah-payment" method="post">
            @csrf
            <div class="mb-3">
                <label for="payment" class="form-label">Payment</label>
                <input type="text" class="form-control" id="payment" name="payment">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

    <a href="/">kembali</a>

@endsection
