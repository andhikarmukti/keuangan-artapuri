@extends('layouts.main')

@section('container')
<h1>halaman analytic</h1>

{{-- @if (request('bulan'))
{{ 'ok' }}
@endif --}}

@if (session()->has('suksesUpdateData'))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    {{ session('suksesUpdateData') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

@if (session()->has('suksesHapus'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{ session('suksesHapus') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

@if (session()->has('delete'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{ session('delete') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

@if (session()->has('edit'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{ session('edit') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

@if (session()->has('tolakAkses'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{ session('tolakAkses') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

<table>
    <form action="analytic" method="get">
        <tr>
            <td>
                <label for="bulan">Bulan & Tahun : </label>
            </td>
            <td>
                <select name="bulan" id="bulan">
                    <option selected disabled>{{ old('bulan') }}</option>
                    <option selected disabled>Bulan</option>
                    <option value="01" @if ($bulan=='01' ) selected @endif>Januari</option>
                    <option value="02" @if ($bulan=='02' ) selected @endif>Februari</option>
                    <option value="03" @if ($bulan=='03' ) selected @endif>Maret</option>
                    <option value="04" @if ($bulan=='04' ) selected @endif>April</option>
                    <option value="05" @if ($bulan=='05' ) selected @endif>Mei</option>
                    <option value="06" @if ($bulan=='06' ) selected @endif>Juni</option>
                    <option value="07" @if ($bulan=='07' ) selected @endif>Juli</option>
                    <option value="08" @if ($bulan=='08' ) selected @endif>Agustus</option>
                    <option value="09" @if ($bulan=='09' ) selected @endif>September</option>
                    <option value="10" @if ($bulan=='10' ) selected @endif>Oktober</option>
                    <option value="11" @if ($bulan=='11' ) selected @endif>November</option>
                    <option value="12" @if ($bulan=='12' ) selected @endif>Desember</option>
                </select>
                <select name="tahun" id="tahun">
                    <option selected disabled>Tahun</option>
                    <option value="2021" @if ($tahun=='2021' ) selected @endif>2021</option>
                    <option value="2022" @if ($tahun=='2022' ) selected @endif>2022</option>
                    <option value="2023" @if ($tahun=='2023' ) selected @endif>2023</option>
                    <option value="2024" @if ($tahun=='2024' ) selected @endif>2024</option>
                </select>
            </td>
        </tr>

        <tr>
            <td>
                <div>
                    <label for="entitas">Entitas : </label>
            </td>
            <td>
                <select name="entitas" id="entitas">
                    <option value="" selected disabled>--</option>
                    @if(auth()->user()->role == 'admin')
                    <option value="All" selected>All</option>
                    @foreach ($enttitas_admin as $e_a)
                    <option value="{{ $e_a->entitas }}" @if(request('entitas')==$e_a->entitas) selected @endif>{{
                        $e_a->entitas }}</option>
                    @endforeach
                    @endif
                    @foreach($entitas as $entits)
                    <option value="{{ $entits->jenis_akses }}" @if(request('entitas')==$entits->jenis_akses) selected
                        @endif>{{ $entits->jenis_akses }}</option>
                    @endforeach
                </select>
            </td>
            </div>
        </tr>

        <tr>
            <td>
                <div>
                    <label for="kategori">Kategori : </label>
            </td>
            <td>
                <select name="kategori" id="kategori">
                    <option value="" selected disabled>--</option>
                </select>
                </div>
            </td>
        </tr>

        <tr>
            <td>
                <div>
                    <label for="keterangan">Keterangan: </label>
            </td>
            <td>
                <input type="text" name="keterangan" id="keterangan" value="{{ request('keterangan') }}">
                </div>
            </td>
        </tr>

        <tr>
            <th>
                <button type="submit" id="tombolCari" class="btn btn-primary">Cari</button>
                <a href="/analytic" class="btn btn-danger">Reset</a>
            </th>
        </tr>
    </form>
</table>

<div class="card mt-4 mb-3 d-flex justify-content-center" style="width: 18rem;">
    <ul class="list-group list-group-flush">
        <li class="list-group-item"><b>Total Pemasukan : Rp. {{ number_format($totalPemasukan,0,'.','.') }}</b></li>
        <li class="list-group-item"><b>Total Pengeluaran : Rp. {{ number_format($totalPengeluaran,0,'.','.') }}</b></li>
        <li class="list-group-item"><b>Selisih : <b @if($totalPemasukan-$totalPengeluaran < 0) style="color:red" @else
                    style="color:green" @endif> {{ number_format($totalPemasukan-$totalPengeluaran,0,'.','.') }}</b>
        </li>
    </ul>
</div>
@if(auth()->user()->role == 'admin' || request('entitas'))
<table class="table table-border table-hover" id="table">
    <thead>
        <tr>
            <th scope="col">Tanggal</th>
            <th scope="col">Jenis</th>
            <th scope="col">Entitas</th>
            <th scope="col">Kategori</th>
            <th scope="col">Currency</th>
            <th scope="col">Nominal</th>
            <th scope="col">Payment</th>
            <th scope="col">Keterangan</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>

        @if($data->count() == 0)
        <tr>
            <td colspan="9" style="text-align:center;color:red;font-style: italic">khosoong</td>
        </tr>
        @endif

        @foreach ($data as $d)
        <tr>
            {{-- date('Y-M-D', strtotime($d->tanggal)) --}}
            <td>{{ $d->tanggal }}</td>
            <td>{{ $d->jenis }}</td>
            <td>{{ $d->entitas }}</td>
            <td>{{ $d->kategori }}</td>
            <td>{{ $d->currency }}</td>
            <td>{{ number_format($d->nominal,0,'.','.') }}</td>
            <td>{{ $d->payment }}</td>
            <td>{{ $d->keterangan }}</td>
            <td>
                <a href="/cashflow/edit/{{ $d->id }}"><span class="badge bg-warning">edit</span></a>
                <a href="/cashflow/delete/{{ $d->id }}" onclick="return confirm('yakin?')">
                    <span class="badge bg-danger">delete</span>
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>

</table>

{{-- <div class="d-flex justify-content-center">
    {{ $data->links() }}
</div> --}}
<br>
<br>
<hr>
<br>

<div class="panel">
    <div id="piechart"></div>
</div>
@endif

<script src="https://code.highcharts.com/highcharts.src.js"></script>

{{ request('kategori') }}
</body>

<script type="text/javascript">
    Highcharts.chart('piechart', {
    chart: {
        type: 'pie'
    },
    title: {
        text: 'Total Pemasukan & Pengeluaran'
    },
    subtitle: {
        text: 'PT. Artapuri Digital Mediatama'
    },

    accessibility: {
        announceNewData: {
            enabled: true
        },
        point: {
            valueSuffix: '%'
        }
    },

    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.name}: {point.currency} {point.y:,.,.0f}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: {point.currency} <b>{point.y:,.0f}'
    },

    series: [
        {
            name: "Total",
            colorByPoint: true,
            data: [
                {
                    name: "Pemasukan",
                    y: {!! json_encode($totalPemasukan) !!},
                    currency: 'Rp ',
                    drilldown: "Chrome"
                },
                {
                    name: "Pengeluaran",
                    y: {!! json_encode($totalPengeluaran) !!},
                    currency: 'Rp ',
                    drilldown: "Firefox"
                }
            ]
        }
    ],
    drilldown: {
        series: [
            {
                name: "Chrome",
                id: "Chrome",
                data: [
                    [
                        "v65.0",
                        0.1
                    ],
                    [
                        "v64.0",
                        1.3
                    ]
                ]
            },
            {
                name: "Firefox",
                id: "Firefox",
                data: [
                    [
                        "v58.0",
                        1.02
                    ],
                    [
                        "v57.0",
                        7.36
                    ]
                ]
            }
        ]
    }
});
</script>

{{-- <script>
    $('.detail').click(function(){
        var id = $(this).attr('value')
        console.log(id)
        $.ajax({
            type : 'GET',
            url : '/analytic?nilai='+id,
            typeData : 'JSON',
            success : function(result){
                console.log(result)
            }
        });
    });
</script> --}}

<script>
    $('.delete').click(function(e){

        e.preventDefault();
        var href = $(this).attr('href');

        Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
        if (result.isConfirmed) {
            document.location.href = href;
            Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
            )
        }
        })
    })

    $( document ).ready(function(){
            var nilai = $('#entitas').val();
            $.ajax({
                type : 'GET',
                url : '/cashflow?nilai='+nilai,
                dataType : 'JSON',
                success : function(result){
                    // console.log(result)
                    if(result.length > 0){
                        $('#kategori').empty();
                        $('#kategori').html(`
                        <option @if (!request('kategori')) selected @endif disabled >Pilih Kategori</option>
                        `);
                    $.each(result, function(index, element){
                        // console.log(element)
                        if('{{ request('kategori') }}' == element){
                            $('#kategori').append(`
                                <option value = "`+ element +`" selected>`+ element +`</option>
                            `)
                        }else{
                            $('#kategori').append(`
                                <option value = "`+ element +`" @if (request('kategori') == '`+ element +`') selected @endif>`+ element +`</option>
                            `)
                        }
                        })
                    }else{
                        $('#kategori').html(`
                            <option selected disabled>--</option>
                        `)
                    }
                }
            })

            $('#entitas').change(function(){
            var nilai = $(this).val();
            $.ajax({
                type : 'GET',
                url : '/cashflow?nilai='+nilai,
                dataType : 'JSON',
                success : function(result){
                    console.log(result)
                    if(result.length > 0){
                        $('#kategori').empty();
                        $('#kategori').html(`
                            <option selected disabled >Pilih Kategori</option>
                        `);
                    $.each(result, function(index, element){
                            $('#kategori').append(`
                                <option value = '`+ element +`'>`+ element +`</option>
                            `)
                        })
                    }else{
                        $('#kategori').html(`
                            <option selected disabled>Tidak Ada Kategori</option>
                        `)
                    }
                }
            })
       });
       });
</script>
@endsection