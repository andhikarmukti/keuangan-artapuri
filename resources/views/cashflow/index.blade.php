@extends('layouts.main')

@section('container')

<div class="col-6 mt-5 mb-4">
    <form action="/cashflow" method="post">
        @csrf
        <div class="mb-3">
            <label for="tanggal" class="form-label">Tanggal</label>
            <input type="date" class="form-control @error('tanggal') is-invalid @enderror" id="tanggal" name="tanggal" value="{{ old('tanggal', $now) }}">
            @error('tanggal')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="jenis" class="form-label">Jenis</label>
            <select class="form-select @error('jenis') is-invalid @enderror" name="jenis" id="jenis">
                <option selected value="{{ old('jenis') }}">{{ old('jenis') }}</option>
                <option value="Pemasukan">Pemasukan</option>
                <option value="Pengeluaran">Pengeluaran</option>
            </select>
            @error('jenis')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="entitas" class="form-label">entitas</label>
            <select class="form-select @error('entitas') is-invalid @enderror" name="entitas" id="entitas">
                <option selected value="" disabled>--</option>
                @foreach($entitas as $e)
                <option value="{{ $e->entitas }}" {{ old('entitas') == $e->entitas ? 'selected' : "" }}>{{ $e->entitas }}</option>
                @endforeach
              </select>
              @error('entitas')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="kategori" class="form-label">Kategori</label>
            <select class="form-select @error('kategori') is-invalid @enderror" name="kategori" id="kategori">
                <option selected value="{{ old('kategori') }}">{{ old('kategori') }}</option>
              </select>
              @error('kategori')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="currency" class="form-label">Currency</label>
            <select class="form-select @error('currency') is-invalid @enderror" name="currency" id="currency">
                <option selected disabled value="{{ old('currency') }}">{{ old('currency') }}</option>
                <option @if(old('currency') == 'Rupiah') selected @elseif(!old('currency')) selected @endif value="Rupiah">Rupiah</option>
                <option {{ old('currency') == 'Dollar' ? 'selected' : "" }} value="Dollar">Dollar</option>
            </select>
            @error('currency')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="nominal" class="form-label">Nominal</label>
            <div class="input-group mb-3">
                <span class="input-group-text" id="satuanNominal">Rp</span>
                <input type="int" class="form-control @error('nominal') is-invalid @enderror" placeholder="Nominal" name="nominal" id="nominal" value="{{ old('nominal') }}">
                @error('nominal')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
            </div>
        </div>
        <div class="mb-3">
            <label for="payment" class="form-label">Payment</label>
            <select class="form-select @error('payment') is-invalid @enderror" name="payment" id="payment">
                <option selected value="{{ old('payment') }}">{{ old('payment') }}</option>
                @foreach ($payment as $p)
                <option value="{{ $p->payment }}">{{ $p->payment }}</option>
                @endforeach
            </select>
            @error('payment')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="keterangan" class="form-label">Keterangan</label>
            <textarea class="form-control @error('keterangan') is-invalid @enderror" id="keterangan" name="keterangan">{{ old('keterangan') }}</textarea>
            @error('keterangan')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
<a href="/">kembali</a>

<script>
       $('#entitas').change(function(){
            var nilai = $(this).val();
            $.ajax({
                type : 'GET',
                url : '/cashflow?nilai='+nilai,
                dataType : 'JSON',
                success : function(result){
                    console.log(result)
                    if(result.length > 0){
                        $('#kategori').empty();
                        $('#kategori').html(`
                            <option selected disabled >Pilih Kategori</option>
                        `);
                    $.each(result, function(index, element){
                            $('#kategori').append(`
                                <option value = '`+ element +`'>`+ element +`</option>
                            `)
                        })
                    }else{
                        $('#kategori').html(`
                            <option selected disabled>Tidak Ada Kategori</option>
                        `)
                    }
                }
            })
       });

        // Format mata uang.
        // $( '#nominal' ).mask('000.000.000', {reverse: true});

       $('#currency').change(function(){
            var currency = $(this).val();
            console.log(currency);

           if(currency == 'Rupiah'){
               $('#satuanNominal').html(`
                   <span id="satuanNominal">`+ 'Rp' +`</span>
               `)
           }else {
            $('#satuanNominal').html(`
                   <span id="satuanNominal">$</span>
               `)
           }
       });

</script>


@endsection

