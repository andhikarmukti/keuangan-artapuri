@extends('layouts.main')

@section('container')

<div class="col-6 mt-5 mb-4">

    @if (session()->has('edit'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
    {{ session('edit') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    @if (session()->has('delete'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
    {{ session('delete') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <form action="/cashflow/edit" method="post">
        @csrf
        <input type="hidden" class="form-control" id="id" name="id" value="{{ $data['id'] }}">
        <input type="hidden" class="form-control" id="urlBack" name="urlBack" value="{{ $urlBack }}">
        <div class="mb-3">
            <label for="tanggal" class="form-label">Tanggal</label>
            <input type="date" class="form-control @error('tanggal') is-invalid @enderror" id="tanggal" name="tanggal" value="{{ $data['tanggal'] }}">
            @error('tanggal')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="jenis" class="form-label">Jenis</label>
            <select class="form-select @error('jenis') is-invalid @enderror" name="jenis" id="jenis">
                <option selected value="{{ $data['jenis'] }}">{{ $data['jenis'] }}</option>
                <option value="Pemasukan">Pemasukan</option>
                <option value="Pengeluaran">Pengeluaran</option>
            </select>
            @error('jenis')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="entitas" class="form-label">entitas</label>
            <select class="form-select @error('entitas') is-invalid @enderror" name="entitas" id="entitas">
                <option selected value="" disabled>--</option>
                @foreach($entitas as $e)
                <option value="{{ $e->entitas }}" {{ $data['entitas'] == $e->entitas ? 'selected' : "" }}>{{ $e->entitas }}</option>
                @endforeach
              </select>
              @error('entitas')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="kategori" class="form-label">Kategori</label>
            <select class="form-select @error('kategori') is-invalid @enderror" name="kategori" id="kategori">
                <option selected value="{{ $data['kategori'] }}">{{ $data['kategori'] }}</option>
              </select>
              @error('kategori')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="currency" class="form-label">Currency</label>
            <select class="form-select @error('currency') is-invalid @enderror" name="currency" id="currency">
                <option selected disabled value="{{ $data['currency'] }}">{{ $data['currency'] }}</option>
                <option {{ $data['currency'] == 'Rupiah' ? 'selected' : "" }} value="Rupiah">Rupiah</option>
                <option {{ $data['currency'] == 'Dollar' ? 'selected' : "" }} value="Dollar">Dollar</option>
            </select>
            @error('currency')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="nominal" class="form-label">Nominal</label>
            <div class="input-group mb-3">
                <span class="input-group-text" id="satuanNominal">{{ $data['currency'] == 'Rupiah' ? 'Rp' : '$' }}</span>
                <input type="int" class="form-control @error('nominal') is-invalid @enderror" placeholder="Nominal" name="nominal" id="nominal" value="{{ $data['nominal'] }}">
                @error('nominal')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
            </div>
        </div>
        <div class="mb-3">
            <label for="payment" class="form-label">Payment</label>
            <select class="form-select @error('payment') is-invalid @enderror" name="payment" id="payment">
                <option selected value="{{ $data['payment'] }}">{{ $data['payment'] }}</option>
                @foreach ($payment as $p)
                <option value="{{ $p->payment }}">{{ $p->payment }}</option>
                @endforeach
            </select>
            @error('payment')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="keterangan" class="form-label">Keterangan</label>
            <textarea class="form-control @error('keterangan') is-invalid @enderror" id="keterangan" name="keterangan">{{ $data['keterangan'] }}</textarea>
            @error('keterangan')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
<a href="{{ URL::previous() }}">kembali</a>

<script>
       $('#entitas').change(function(){
            var nilai = $(this).val();
            $.ajax({
                type : 'GET',
                url : '/cashflow?nilai='+nilai,
                dataType : 'JSON',
                success : function(result){
                    console.log(result)
                    if(result.length > 0){
                        $('#kategori').empty();
                        $('#kategori').html(`
                            <option selected disabled >Pilih Kategori</option>
                        `);
                    $.each(result, function(index, element){
                            $('#kategori').append(`
                                <option value = '`+ element +`'>`+ element +`</option>
                            `)
                        })
                    }else{
                        $('#kategori').html(`
                            <option selected disabled>Tidak Ada Kategori</option>
                        `)
                    }
                }
            })
       });

        // Format mata uang.
        // $( '#nominal' ).mask('000.000.000', {reverse: true});

       $('#currency').change(function(){
            var currency = $(this).val();
            console.log(currency);

           if(currency == 'Rupiah'){
               $('#satuanNominal').html(`
                   <span id="satuanNominal">`+ 'Rp' +`</span>
               `)
           }else {
            $('#satuanNominal').html(`
                   <span id="satuanNominal">$</span>
               `)
           }
       });

</script>

<script type="text/javascript">
		
    var nominal = document.getElementById('nominal');
    nominal.addEventListener('keyup', function(e){
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        nominal.value = formatRupiah(this.value);
    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        nominal     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            nominal += separator + ribuan.join('.');
        }

        nominal = split[1] != undefined ? nominal + ',' + split[1] : nominal;
        return prefix == undefined ? nominal : (nominal ? '' + nominal : '');
    }
</script>


@endsection

