@extends('layouts.main')

@section('container')
    <a href="/activate-account">kembali</a>
    <h3 class="mt-5 mb-3">Edit : {{ $user->name }}</h3>
    <div class="col-2">
        <form method="POST" action="/activate-account/edit">
            @csrf
            @method('put')
            <input type="hidden" value="{{ $user->id }}" name="id" id="id">
            <div class="mb-3">
              <label for="role" class="form-label">Role</label>
              <select class="form-select" id="role" name="role">
                <option value="staff" {{ $user->role == 'staff' ? 'selected' : '' }}>staff</option>
                <option value="admin" {{ $user->role == 'admin' ? 'selected' : '' }}>admin</option>
              </select>
            </div>
            <div class="mb-3">
              <label for="activation" class="form-label">Activation</label>
              <select class="form-select" id="activation" name="activation">
                <option value="1" {{ $user->active == 1 ? 'selected' : '' }}>active</option>
                <option value="0" {{ $user->active == 0 ? 'selected' : '' }}>disable</option>
              </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
    </div>
@endsection