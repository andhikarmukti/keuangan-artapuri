@extends('layouts.main')

@section('container')
    <a href="/">kembali</a>

    {{-- Alert --}}
    <div class="col-6 mt-3">
      @if (session()->has('update'))
        <div class="alert alert-primary alert-dismissible fade show" role="alert">
            {{ session('update') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
    </div>
    <div class="col-6 mt-3">
      @if (session()->has('delete'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('delete') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
    </div>

    <h3 class="mt-5 mb-3">User List</h3>
    <table class="table table-hover">
        <thead>
          <tr>
            <th scope="col">Name</th>
            {{-- <th scope="col">Email</th> --}}
            <th scope="col">Role</th>
            <th scope="col">Activation</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
            <tr>
                <th scope="row">{{ $user->name }}</th>
                {{-- <td>{{ $user->email }}</td> --}}
                <td>{{ $user->role }}</td>
                <td class="{{ $user->active == 1 ? 'text-success' : 'text-danger' }}">{{ $user->active == 1 ? 'active' : 'disable' }}</td>
                <td>
                    <a href="activate-account/edit/{{ $user->id }}" class="badge bg-warning text-decoration-none">edit</a>
                    <a href="activate-account/delete/{{ $user->id }}" class="badge bg-danger text-decoration-none" onclick="return confirm('are you sure?')">delete</a>
                </td>
              </tr>      
            @endforeach
        </tbody>
      </table>
@endsection