<?php

namespace App\Http\Controllers;

use App\Models\Entitas;
use Illuminate\Http\Request;

class EntitasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('entitas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'entitas' => 'required'
        ]);

        Entitas::create($validatedData);

        return redirect('/')->with('suksesEntitas', 'Berhasil menambahkan entitas ' . $request->entitas);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Entitas  $entitas
     * @return \Illuminate\Http\Response
     */
    public function show(Entitas $entitas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Entitas  $entitas
     * @return \Illuminate\Http\Response
     */
    public function edit(Entitas $entitas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Entitas  $entitas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Entitas $entitas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Entitas  $entitas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entitas $entitas)
    {
        //
    }
}
