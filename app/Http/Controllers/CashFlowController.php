<?php

namespace App\Http\Controllers;

use App\Models\Entitas;
use App\Models\Payment;
use App\Models\CashFlow;
use App\Models\Category;
use App\Models\HakAkses;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class CashFlowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->nilai){
            if($request->nilai == 'All'){
                $kategori = Category::all()->pluck('kategori');
                return response()->json($kategori);
            }
            $kategori = Category::where('entitas', $request->nilai)->pluck('kategori');
            return response()->json($kategori);
        }

        $data = CashFlow::all();
        $entitas = Entitas::all();
        $payment = Payment::all();
        $now = Carbon::now()->format('Y-m-d');

        return view('cashflow.index',[
            'data' => $data,
            'entitas' => $entitas,
            'payment' => $payment,
            'now' => $now
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tanggal' => 'required',
            'jenis' => 'required',
            'entitas' => 'required',
            'kategori' => 'required',
            'currency' => 'required',
            'nominal' => 'required',
            'payment' => 'required',
            'keterangan' => 'required'
        ]);

        CashFlow::create([
            'update_by' => auth()->user()->name,
            'tanggal' => $request->tanggal,
            'jenis' => $request->jenis,
            'entitas' => $request->entitas,
            'kategori' => $request->kategori,
            'currency' => $request->currency,
            'nominal' => str_replace('.','',$request->nominal),
            'payment' => $request->payment,
            'keterangan' => $request->keterangan
        ]);

        return redirect('/')->with('suksesTambahData', 'Data baru berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CashFlow  $cashFlow
     * @return \Illuminate\Http\Response
     */
    public function show(CashFlow $cashFlow)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CashFlow  $cashFlow
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cek_akses = CashFlow::where('id', $id)->get()->pluck('entitas')->toArray();
        $hak_akses = HakAkses::where('user_id', auth()->user()->id)->get();
        foreach($hak_akses as $h_a){
            if(strtolower($h_a->jenis_akses) == strtolower($cek_akses[0])){
                $data = CashFlow::where('id', $id)->get()->toArray();
                $entitas = Entitas::all();
                $payment = Payment::all();
                $urlBack = redirect()->back()->getTargetUrl();
        
                return view('cashflow.edit',[
                    'data' => $data[0],
                    'entitas' => $entitas,
                    'payment' => $payment,
                    'urlBack' => $urlBack
                ]);
            }
        }
        
        return redirect('/')->with('tolakAkses', 'Maaf, Anda tidak punya akses untuk itu');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CashFlow  $cashFlow
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'tanggal' => 'required',
            'jenis' => 'required',
            'entitas' => 'required',
            'kategori' => 'required',
            'currency' => 'required',
            'nominal' => 'required',
            'payment' => 'required',
            'keterangan' => 'required'
        ]);

        CashFlow::where('id', $request->id)->update([
            'update_by' => auth()->user()->name,
            'tanggal' => $request->tanggal,
            'jenis' => $request->jenis,
            'entitas' => $request->entitas,
            'kategori' => $request->kategori,
            'currency' => $request->currency,
            'nominal' => str_replace('.','',$request->nominal),
            'payment' => $request->payment,
            'keterangan' => $request->keterangan
        ]);

        return redirect($request->urlBack)->with('edit', 'Data berhasil diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CashFlow  $cashFlow
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cek_akses = CashFlow::where('id', $id)->get()->pluck('entitas')->toArray();
        $hak_akses = HakAkses::where('user_id', auth()->user()->id)->get();
        foreach($hak_akses as $h_a){
            if($h_a->jenis_akses == $cek_akses[0]){
                CashFlow::destroy($id);

                return redirect()->back()->with('delete', 'Data berhasil dihapus');
            }
        }
        return redirect('/')->with('tolakAkses', 'Maaf, Anda tidak punya akses untuk itu');
    }
}
