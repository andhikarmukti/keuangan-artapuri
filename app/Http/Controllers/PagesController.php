<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Entitas;
use App\Models\Payment;
use App\Models\CashFlow;
use App\Models\Category;
use App\Models\HakAkses;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class PagesController extends Controller
{
    public function home(Request $request)
    {
        $data = CashFlow::latest()->paginate(20)->withQueryString();
        if($request->search){
            $data = CashFlow::where('jenis', 'LIKE', '%' . $request->search . '%')
            ->orWhere('entitas', 'LIKE', '%' . $request->search . '%')
            ->orWhere('kategori', 'LIKE', '%' . $request->search . '%')
            ->orWhere('keterangan', 'LIKE', '%' . $request->search . '%')
            ->orWhere('payment', 'LIKE', '%' . $request->search . '%')
            ->paginate(20)->withQueryString();
        }

        $now = Carbon::now()->format('Y-m-d');
        $entitas = HakAkses::where('user_id', auth()->user()->id)->get();
        $enttitas_admin = Entitas::all();
        $payment = Payment::all();

        if(Carbon::now()->format('H:i:s') <= "15:00" && Carbon::now()->format('H:i:s') >= "11:00"){
            $greetings = "Selamat siang";
        }elseif(Carbon::now()->format('H:i:s') <= "10:00" && Carbon::now()->format('H:i:s') >= "04:00"){
            $greetings = "Selamat pagi";
        }elseif(Carbon::now()->format('H:i:s') <= "18:00" && Carbon::now()->format('H:i:s') >= "15:00"){
            $greetings = "Selamat sore";
        }else{
            $greetings = "Selamat malam";
        }
        
        return view('home',[
            'data' => $data,
            'now' => $now,
            'entitas' => $entitas,
            'payment' => $payment,
            'greetings' => $greetings,
            'enttitas_admin' => $enttitas_admin
        ]);
    }

    public function analytic(Request $request)
    {
        $data = [];
        $totalPemasukan = 0;
        $totalPengeluaran = 0;
        
        
        $entitas = HakAkses::where('user_id', auth()->user()->id)->get();
        foreach($entitas as $e){
            if($e->jenis_akses == $request->entitas){
                if ($request->entitas || $request->kategori || $request->keterangan) {
                    $data = DB::table('cash_flows')
                        ->whereMonth('tanggal', request('bulan'))
                        ->whereYear('tanggal', request('tahun'))
                        ->where('entitas', 'like', '%' . $request->entitas . '%')
                        ->where('kategori', 'like', '%' . $request->kategori . '%')
                        ->where('keterangan', 'like', '%' . $request->keterangan . '%')
                        ->get()->sortBy('tanggal');
                    $totalPemasukan = CashFlow::where('tanggal', 'like', '%' . request('bulan') . '%')
                        ->where('tanggal', 'like', '%' . request('tahun') . '%')
                        ->where('jenis', 'Pemasukan')
                        ->where('entitas', 'like', '%' . $request->entitas . '%')
                        ->where('kategori', 'like', '%' . $request->kategori . '%')
                        ->pluck('nominal')
                        ->sum();
                    $totalPengeluaran = CashFlow::where('tanggal', 'like', '%' . request('bulan') . '%')
                        ->where('tanggal', 'like', '%' . request('tahun') . '%')
                        ->where('jenis', 'Pengeluaran')
                        ->where('entitas', 'like', '%' . $request->entitas . '%')
                        ->where('kategori', 'like', '%' . $request->kategori . '%')
                        ->pluck('nominal')
                        ->sum();
                }
            }
        }

        if(auth()->user()->role == 'admin'){
            if($request->entitas == 'All'){
                $data = DB::table('cash_flows')
                    ->whereMonth('tanggal', request('bulan'))
                    ->whereYear('tanggal', request('tahun'))
                    ->where('kategori', 'like', '%' . $request->kategori . '%')
                    ->where('keterangan', 'like', '%' . $request->keterangan . '%')
                    ->get()->sortBy('tanggal');
            }
        }

        $cashflow = CashFlow::where('entitas', 'Artapuri')->get();
        $categories = Category::all();
        $enttitas_admin = Entitas::all();
        $bulan = $request->bulan;
        $tahun = $request->tahun;

        if (request('bulan') ==  null) {
            $totalPemasukan = 0;
            $totalPengeluaran = 0;
            $bulan = Carbon::now()->format('m');
            $tahun = Carbon::now()->format('Y');
        }

        if(auth()->user()->role == 'admin'){
            if ($request->entitas || $request->kategori || $request->keterangan) {
                if($request->entitas == 'All'){
                    $data = DB::table('cash_flows')
                        ->whereMonth('tanggal', request('bulan'))
                        ->whereYear('tanggal', request('tahun'))
                        ->where('kategori', 'like', '%' . $request->kategori . '%')
                        ->where('keterangan', 'like', '%' . $request->keterangan . '%')
                        ->get()->sortBy('tanggal');
                    $totalPemasukan =DB::table('cash_flows')->whereMonth('tanggal', request('bulan'))
                        ->whereYear('tanggal', request('tahun'))
                        ->where('jenis', 'Pemasukan')
                        ->where('kategori', 'like', '%' . $request->kategori . '%')
                        ->get()
                        ->pluck('nominal')
                        ->sum();
                    $totalPengeluaran =DB::table('cash_flows')->whereMonth('tanggal', request('bulan'))
                        ->whereYear('tanggal', request('tahun'))
                        ->where('jenis', 'Pengeluaran')
                        ->where('kategori', 'like', '%' . $request->kategori . '%')
                        ->get()
                        ->pluck('nominal')
                        ->sum();
                }else{
                    $data = DB::table('cash_flows')
                        ->whereMonth('tanggal', request('bulan'))
                        ->whereYear('tanggal', request('tahun'))
                        ->where('entitas', 'like', '%' . $request->entitas . '%')
                        ->where('kategori', 'like', '%' . $request->kategori . '%')
                        ->where('keterangan', 'like', '%' . $request->keterangan . '%')
                        ->get()->sortBy('tanggal');
                    $totalPemasukan =DB::table('cash_flows')->whereMonth('tanggal', request('bulan'))
                        ->whereYear('tanggal', request('tahun'))
                        ->where('jenis', 'Pemasukan')
                        ->where('entitas', 'like', '%' . $request->entitas . '%')
                        ->where('kategori', 'like', '%' . $request->kategori . '%')
                        ->get()
                        ->pluck('nominal')
                        ->sum();
                    $totalPengeluaran =DB::table('cash_flows')->whereMonth('tanggal', request('bulan'))
                        ->whereYear('tanggal', request('tahun'))
                        ->where('jenis', 'Pengeluaran')
                        ->where('entitas', 'like', '%' . $request->entitas . '%')
                        ->where('kategori', 'like', '%' . $request->kategori . '%')
                        ->get()
                        ->pluck('nominal')
                        ->sum();
                }
            }else{
                $data = DB::table('cash_flows')->whereMonth('tanggal', Carbon::now()->format('m'))
                    ->whereYear('tanggal', Carbon::now()->format('Y'))
                    ->get()->sortBy('tanggal');
                $totalPemasukan =DB::table('cash_flows')->whereMonth('tanggal', Carbon::now()->format('m'))
                    ->whereYear('tanggal', Carbon::now()->format('Y'))
                    ->where('jenis', 'Pemasukan')
                    ->where('entitas', 'like', '%' . $request->entitas . '%')
                    ->where('kategori', 'like', '%' . $request->kategori . '%')
                    ->get()
                    ->pluck('nominal')
                    ->sum();
                $totalPengeluaran =DB::table('cash_flows')->whereMonth('tanggal', Carbon::now()->format('m'))
                    ->whereYear('tanggal', Carbon::now()->format('Y'))
                    ->where('jenis', 'Pengeluaran')
                    ->where('entitas', 'like', '%' . $request->entitas . '%')
                    ->where('kategori', 'like', '%' . $request->kategori . '%')
                    ->get()
                    ->pluck('nominal')
                    ->sum();
            }
        }

        return view('analytic.index', [
            'data' => $data,
            'bulan' => $bulan,
            'tahun' => $tahun,
            'cashflow' => $cashflow,
            'totalPemasukan' => $totalPemasukan,
            'totalPengeluaran' => $totalPengeluaran,
            'entitas' => $entitas,
            'enttitas_admin' => $enttitas_admin,
            'categories' => $categories
        ]);
    }
}
