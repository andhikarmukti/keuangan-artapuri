<?php

namespace App\Http\Controllers;

use App\Models\Entitas;
use App\Models\User;
use App\Models\HakAkses;
use Illuminate\Http\Request;

class HakAksesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = User::where('role', 'staff')->get();
        $entitas = Entitas::all();
        $hak_akses = HakAkses::where('user_id', $request->user_id)->get();
        
        return view('hak-akses.create',compact([
            'user',
            'entitas',
            'hak_akses'
        ]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        HakAkses::where('user_id', $request->user_id)->delete();
        $entitas = Entitas::all();
        foreach($entitas as $e){
            $jenis_akses = $e->entitas;
            if($request->$jenis_akses == $e->entitas){
                HakAkses::create([
                    'user_id' => $request->user_id,
                    'jenis_akses' => $request->$jenis_akses
                ]);
            }
        }
        
        return back()->with('berhasil', 'Berhasil melakukan perubahan hak akses');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HakAkses  $hakAkses
     * @return \Illuminate\Http\Response
     */
    public function show(HakAkses $hakAkses)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HakAkses  $hakAkses
     * @return \Illuminate\Http\Response
     */
    public function edit(HakAkses $hakAkses)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HakAkses  $hakAkses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HakAkses $hakAkses)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HakAkses  $hakAkses
     * @return \Illuminate\Http\Response
     */
    public function destroy(HakAkses $hakAkses)
    {
        //
    }
}
