<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function activate_account()
    {
        $users = User::where('name', '!=', 'dika')->get();
        
        return view('user.activate-account', compact(
            'users'
        ));
    }

    public function activate_account_edit($id)
    {
        $user = User::find($id);
        
        return view('user.activate-account-edit', compact(
            'user'
        ));
    }

    public function activate_account_update(Request $req)
    {
        $name = User::find($req->id)->name;

        User::where('id', $req->id)->update([
            'role' => $req->role,
            'active' => $req->activation
        ]); return redirect('/activate-account')->with('update', 'Berhasil melakukan perubahan untuk user ' . $name);
    }

    public function activate_account_delete($id)
    {
        $name = User::find($id)->name;

        User::destroy($id);
        return redirect('/activate-account')->with('delete', 'Berhasil menghapus user ' . $name);
    }
}
