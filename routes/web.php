<?php

use App\Models\User;
use App\Models\CashFlow;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\EntitasController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\CashFlowController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HakAksesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::get('/', [PagesController::class, 'home'])->middleware('auth', 'active');

// cashflow
Route::get('/cashflow', [CashFlowController::class, 'index'])->middleware('auth', 'active');
Route::get('/cashflow/edit/{id}', [CashFlowController::class, 'edit'])->middleware('auth', 'active');
Route::post('/cashflow/edit', [CashFlowController::class, 'update'])->middleware('auth', 'active');
Route::get('/cashflow/delete/{id}', [CashFlowController::class, 'destroy'])->middleware('auth', 'active');
Route::post('/cashflow', [CashFlowController::class, 'store'])->middleware('auth', 'active');

// analytic
Route::get('/analytic', [PagesController::class, 'analytic'])->middleware('auth', 'active');

// tambah tambah
Route::get('/tambah-kategori', [CategoryController::class, 'index'])->middleware('auth', 'active');
Route::post('/tambah-kategori', [CategoryController::class, 'store'])->middleware('auth', 'active');
Route::get('/tambah-entitas', [EntitasController::class, 'index'])->middleware('auth', 'active', 'admin');
Route::post('/tambah-entitas', [EntitasController::class, 'store'])->middleware('auth', 'active', 'admin');
Route::get('/tambah-payment', [PaymentController::class, 'index'])->middleware('auth', 'active');
Route::post('/tambah-payment', [PaymentController::class, 'store'])->middleware('auth', 'active');

// Hak Akses
Route::get('/hak-akses', [HakAksesController::class, 'create'])->middleware('auth', 'active', 'admin');
Route::post('/hak-akses', [HakAksesController::class, 'store'])->middleware('auth', 'active', 'admin');

// Aktivasi User Baru
Route::get('/activate-account', [UserController::class, 'activate_account'])->middleware('auth', 'active', 'admin');
Route::get('/activate-account/edit/{id}', [UserController::class, 'activate_account_edit'])->middleware('auth', 'active', 'admin');
Route::put('/activate-account/edit', [UserController::class, 'activate_account_update'])->middleware('auth', 'active', 'admin');
Route::get('/activate-account/delete/{id}', [UserController::class, 'activate_account_delete'])->middleware('auth', 'active', 'admin');